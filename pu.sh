#!/usr/bin/env sh
set -x

cd output/raw
tar czvf - . | ssh root@e9d.org "(cd /srv/http; rm -r ./**; tar xzv; chown -R http:http /srv/http)"
