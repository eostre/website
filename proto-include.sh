#!/usr/bin/env sh
set -x

function proto_include {
	arg=$1 file=$2
	awk "/PROTO/{while(getline line<\"proto/$arg\"){print line}} //" $FILE | sed '/PROTO/d'
}

FILE=$1

PROTO_DIRECTIVE=$(awk -v RS='%PROTO' '{print $1}' $FILE | tail -n 1)
PROTO_ARG=$(awk -v RS='%PROTO' '{print $2}' $FILE | tail -n 1)

case $PROTO_DIRECTIVE in
	INCLUDE)
		touch output/$FILE
		proto_include $PROTO_ARG $FILE > \
			output/$FILE
		;;
	*)
		;;
esac
