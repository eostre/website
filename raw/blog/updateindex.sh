#!/usr/bin/env sh
set -x

cat index.html.proto > index.html

for POST in [000..999]*.html; do
	cat << EOF
	&nbsp;<a href="$POST">$POST</a><br>
EOF
done >> index.html

cat index.html.end >> index.html
